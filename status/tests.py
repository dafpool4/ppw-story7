"""from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone
from .models import StatusModel
from .views import status
from .forms import StatusForm

class StatusUnitTest(TestCase):
    def test_status_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_status_using_status_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, status)

    def test_status_template_used(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'status.html')

    def test_status_object_to_database(self):
        StatusModel.objects.create(content='hai', date=timezone.now())
        count_status = StatusModel.objects.all().count()
        self.assertEqual(count_status, 1)

    def test_form_validated(self):
        form = StatusForm(data={'content':'', 'date':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['your_status'],
            ['This field is required.']
        )

    def test_status_page_is_completed(self):
        test_str = 'hai'
        response_post = Client().post('/status/', {'content': test_str, 'date': timezone.now()})
        self.assertEqual(response_post.status_code, 200)
		
from selenium import webdriver
import unittest
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class NewVisitorTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		self.selenium  = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
		super(NewVisitorTest, self).setUp()
	
	def tearDown(self):
		self.selenium.quit()
		super(NewVisitorTest, self).tearDown()
		
	def test_input_todo(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000')
		
		new_title = selenium.title
		self.assertEqual(new_title,"SEE STATUS")
		
		new_status = selenium.find_element_by_id('id_your_status')
		new_status.send_keys('hallo all')
		new_status.submit()
		time.sleep(5)
		
	def test_css_header_text_color(self):
		welcometext = self.selenium.find_element_by_tag_name('h1')
		self.assertEqual(welcometext.value_of_css_property("color")," #ff1f1f ")

	
""" 